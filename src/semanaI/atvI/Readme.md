# Projeto de Programação

Este projeto contém duas atividades relacionadas à matemática e lógica de programação. Abaixo estão os detalhes de cada atividade:

## Atividade I - Seleção de Quadrante

Em Matemática, existe um problema comum que envolve a determinação do quadrante em que um ponto (coordenadas x, y) se encontra em um plano cartesiano. O plano cartesiano possui 4 quadrantes distintos.

### Descrição da Atividade

A atividade consiste em discutir e corrigir uma observação incorreta relacionada à numeração dos quadrantes. No texto fornecido, foi mencionado que o Quadrante Positivo (x, y) é rotulado como o segundo quadrante, quando, na verdade, deveria ser o primeiro quadrante.
<p align="center">
<img src="img/Quadrante.png" width="30%">
</p>

### Fluxograma - Seleção de Quadrante

Aqui está o fluxograma que ilustra o processo de seleção de quadrante:
<p align="center">
<img src="img/fluxograma.png" width="80%">
</p>

### Execução - Seleção de Quadrante

<p align="center">
<img src="img/output.png" width="80%">
</p>

## Atividade II - Último Dígito Fatorial

O segundo exercício consiste em calcular o último dígito de um número fatorial.

### Descrição da Atividade

O fatorial de um número N, representado por N!, é o produto de todos os números de N até 1, incluindo N. Por exemplo, 3! = 3 * 2 * 1 = 6.

Em alguns casos, o resultado do fatorial pode ser muito grande. Por isso, em vez de mostrar todo o resultado, é necessário mostrar apenas o último dígito de N!.

Assume-se que N está na base 10.

### Fluxograma - Último Dígito Fatorial

Aqui está o fluxograma que ilustra o processo de cálculo do último dígito do fatorial:

<p align="center">
<img src="img/fluxogramaUltimo.png" width=100%>
</p>

### Execução - Último Dígito Fatorial

<p align="center">
<img src="img/ultimoDigito.png" width="80%">
</p>









