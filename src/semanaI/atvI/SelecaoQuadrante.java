package semanaI.atvI;

import java.util.Scanner;

public class SelecaoQuadrante {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int x = 0;
		int y = 0;
		
		// Leitura das coordenadas x e y
		while (x == 0 || x<-1000 || x>1000) {
			System.out.println("Digite a cordenada X entre -1000 e 1000. X nao pode ser 0:");
			x = scanner.nextInt();
		}
		while (y == 0) {
			System.out.println("Digite a cordenada y entre -1000 e 1000. y nao pode ser 0::");
			y = scanner.nextInt();
		}
		
		
		// Verificação do quadrante
		int quadrante;
		if (x > 0 && y > 0) {
			quadrante = 1;
		} else if (x < 0 && y > 0) {
			quadrante = 2;
		} else if (x < 0 && y < 0) {
			quadrante = 3;
		} else {
			quadrante = 4;
		}

		// Saída do quadrante
		System.out.println("Esta no "+quadrante+" quadrante!");
	}
}
