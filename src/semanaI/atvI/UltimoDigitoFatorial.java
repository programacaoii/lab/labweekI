package semanaI.atvI;

import java.util.Scanner;

public class UltimoDigitoFatorial {

	public static int lastDigitFactorial(int N) {
		int factorial = 1;
		
		if (N == 0) {
			return 1;
		}

		for (int i = 1; i <= N; i++) {
			factorial *= i;
		}

		if (N < 5) {
			printFatorial(N);
		}

		return factorial;
	}

	private static void printFatorial(int N) {
		System.out.print(N);

		for (int i = N - 1; i >= 1; i--) {
			System.out.print(" X " + i);
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int N = 0, T = 0, cont;
		int result;
		// Lendo o número de casos de teste
		while (T < 2 || T > 9) {
			System.out.println("Digite o numero de casos de teste (entre 2 e 9):");
			T = scanner.nextInt();
		}

		// Processando cada caso de teste
		for (int t = 0; t < T; t++) {
			 N = 0;
			
			// Lendo o número para calcular o fatorial
			while (N <= 0) {
				System.out.println("Digite um numero para calcular o fatorial:");
				N = scanner.nextInt();
			}
			System.out.print(N + "! = ");
			result = lastDigitFactorial(N);
			System.out.print(" = " + result);
			System.out.println();

			N = 0;
		}

	}

}
