package semanaI.atvII;

public abstract class CalculaAreaPerimetro {
	public abstract double calcularArea();

	public abstract double calcularPerimetro();

}
