# Anotação Atividade 2

Nesta atividade, o objetivo é calcular a área e o perímetro de figuras geométricas, utilizando conceitos de herança e polimorfismo. Serão criadas classes para representar diferentes formas geométricas, como triângulo, retângulo e círculo.

## Descrição da Atividade

1. Crie uma superclasse ou interface com duas funções abstratas para calcular a área e o perímetro de uma figura.
2. Crie classes dedicadas para as seguintes formas: triângulo, retângulo e círculo. Cada classe deve ter seus próprios atributos (base, altura, raio).
3. Em cada classe implementada, herde da classe base (ou interface) as funções de cálculo de área e cálculo de perímetro. Adicione também uma função que permita imprimir o nome da figura, a área calculada e o perímetro.
4. Converta a classe Triangle em uma classe abstrata e, a partir dessa classe, crie três classes derivadas: Triângulo Equilátero, Triângulo Isósceles e Triângulo Escaleno.
5. Crie uma classe Square que herde da classe Rectangle os atributos e o comportamento.
6. Na função "principal", crie instâncias para cada uma das figuras e chame as funções de cálculo de área e perímetro.
## Diagrama - Calcular Perímetro
<img src="img/CalculaAreaPerimetro.png" width="80%">

## Execução - Calcular Perímetro
<img src="img/outputatv2.png" width="80%">



