package semanaI.atvII;

public class Square extends Retangulo {
    public Square(double lado) {
        super(lado, lado);
    }

    @Override
    public String toString() {
        return "Quadrado, area: " + calcularArea() + " perimetro: " + calcularPerimetro();
    }
}