package semanaI.atvII;

public class Retangulo extends CalculaAreaPerimetro {
	private double base;
	private double altura;

	public Retangulo(double base, double altura) {
		this.base = base;
		this.altura = altura;
	}

	@Override
	public double calcularArea() {
		return base * altura;
	}

	@Override
	public double calcularPerimetro() {
		return 2 * (base + altura);
	}

	@Override
	public String toString() {
		return "Retangulo, area: " + calcularArea() + " perimetro: " + calcularPerimetro();
	}
}
