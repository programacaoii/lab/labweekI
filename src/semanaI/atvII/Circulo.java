package semanaI.atvII;

public class Circulo extends CalculaAreaPerimetro {
	
	    private double raio;

	    public Circulo(double raio) {
	        this.raio = raio;
	    }

	    @Override
	    public double calcularArea() {
	        return Math.PI * raio * raio;
	    }

	    @Override
	    public double calcularPerimetro() {
	        return 2 * Math.PI * raio;
	    }

	    @Override
	    public String toString() {
	        return "Circulo, area: " + String.format("%.3f", calcularArea()) + " perimetro: " + String.format("%.4f", calcularPerimetro());
	    
	}
}
