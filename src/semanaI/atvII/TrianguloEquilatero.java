package semanaI.atvII;

public class TrianguloEquilatero extends Triangulo {
	public TrianguloEquilatero(double lado) {
		super(lado, lado, lado);
	}

	@Override
	public double calcularArea() {
		return (Math.sqrt(3) / 4) * lado1 * lado1;
		
	}
	@Override
	public String toString() {
		return "Triangulo equilatero, area: " +String.format("%.3f", calcularArea()) + " perimetro: " + calcularPerimetro();
	}

	
}