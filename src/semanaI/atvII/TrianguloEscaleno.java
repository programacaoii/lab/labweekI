package semanaI.atvII;

public class TrianguloEscaleno extends Triangulo {
	    public TrianguloEscaleno(double side1, double side2, double side3) {
	        super(side1, side2, side3);
	    }

	    @Override
	    public double calcularArea() {
	        double semiperimetro = calcularPerimetro() / 2;
	        return Math.sqrt(semiperimetro * (semiperimetro - lado1) * (semiperimetro - lado2) * (semiperimetro - lado3));
	    }

	    @Override
	    public String toString() {
	        
	        return "Triângulo escaleno, área: " + String.format("%.3f", calcularArea()) + " perímetro: " + calcularPerimetro();
	    }
	
}
