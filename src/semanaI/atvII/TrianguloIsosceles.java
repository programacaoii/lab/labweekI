package semanaI.atvII;

public class TrianguloIsosceles extends Triangulo {
	
    public TrianguloIsosceles(double equalSide, double base) {
        super(equalSide, equalSide, base);
    }

    @Override
    public double calcularArea() {
        return (lado3 * Math.sqrt(lado1 * lado1 - (lado3 * lado3 / 4))) / 2;
    }

    @Override
    public String toString() {

        return "Triângulo isósceles, área: " + String.format("%.3f", calcularArea()) + " perímetro: " + calcularPerimetro();
    }
}

