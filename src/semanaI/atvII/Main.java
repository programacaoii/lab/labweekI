package semanaI.atvII;

public class Main {
	 public static void main(String[] args) {
		 
	        Circulo circle = new Circulo(5);
	        System.out.println(circle.toString());

	        Retangulo rectangle = new Retangulo(15, 7);
	        System.out.println(rectangle.toString());

	        Triangulo triangle = new TrianguloEquilatero(7);
	        System.out.println(triangle.toString());

	        Square square = new Square(2);
	        System.out.println(square.toString());
	    }
}
