package semanaI.atvIII;

import java.util.ArrayList;
import java.util.List;

public class Main {
	   public static void main(String[] args) {
	        Universidade universidade = new Universidade();

	        Aluno aluno1 = new Aluno("Gery", 1, new ArrayList<>());
	        aluno1.addCurso("induction");
	        universidade.addAluno(aluno1);

	        Aluno aluno2 = new Aluno("Luis", 2, new ArrayList<>());
	        aluno2.addCurso("science");
	        universidade.addAluno(aluno2);

	        Aluno aluno3 = new Aluno("Raul", 3, new ArrayList<>());
	        aluno3.addCurso("science");
	        universidade.addAluno(aluno3);

	        Aluno aluno4 = new Aluno("Liz", 4, new ArrayList<>());
	        aluno4.addCurso("science");
	        universidade.addAluno(aluno4);

	        universidade.addCurso(3, "DataBase I");
	        universidade.promoverNota(1, 2);

	        List<String> alunosDoCurso = universidade.listarAlunosDoCurso("science");
	        System.out.println(alunosDoCurso); // Output: [Luis, Raul, Liz]

	        List<String> todosAlunos = universidade.listarTodosAlunos();
	        for (String aluno : todosAlunos) {
	            System.out.println(aluno);
	        }
	        /* Output:
	           Nome: Gery, Nota: 1, Cursos: [induction]
	           Nome: Luis, Nota: 2, Cursos: [science]
	           Nome: Raul, Nota: 3, Cursos: [science, DataBase I]
	           Nome: Liz, Nota: 4, Cursos: [science]
	        */
	    }
	
}
