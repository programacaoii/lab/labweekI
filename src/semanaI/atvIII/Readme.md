# Anotação Atividade 3

Nesta atividade, será criada uma classe "Aluno" com atributos como nome, nota e uma matriz para os cursos atribuídos. Também será criada uma classe "Universidade" que inclui uma matriz para armazenar os alunos. Serão implementadas funcionalidades como adicionar um novo aluno, atribuir um novo curso a um aluno, promover a nota de um aluno e listar os alunos de um curso específico.

## Descrição da Atividade

1. Crie uma classe "Aluno" com os seguintes atributos: nome, nota e uma matriz para os cursos atribuídos. Adicione modificadores de acesso para que os atributos não sejam acessíveis publicamente. Implemente as funções "set" e "get" para cada um dos atributos.
2. Crie uma classe "Universidade" que inclua uma matriz para os alunos. Implemente as seguintes funcionalidades:
   - Adicionar um novo aluno à lista, por exemplo: `universidade.addAluno(new Aluno("Gery", 1, {"induction"}))`
   - Atribuir um novo curso a um aluno: `alunos[3].addCurso("DataBase I")`
   - Promover a nota de um aluno: `aluno.setNota(2)`
   - Listar os alunos de um curso: `universidade.filter("science")` retornará ["Luis", "Raul", "Liz"]
   - Listar todos os alunos com o seguinte formato: `Nome: Raul, Nota: 2, Curso: maths, science, database I`

## Diagrama - Atividade 3
<img src="img/package.png" width="80%">

## Execução - Atividade 3
<img src="img/outputatv3.png" width="80%">
