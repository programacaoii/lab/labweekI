package semanaI.atvIII;

import java.util.ArrayList;
import java.util.List;

public class Universidade {
    private List<Aluno> alunos;

    public Universidade() {
        alunos = new ArrayList<>();
    }

    public void addAluno(Aluno aluno) {
        alunos.add(aluno);
    }

    public void addCurso(int alunoIndex, String curso) {
        Aluno aluno = alunos.get(alunoIndex);
        aluno.addCurso(curso);
    }

    public void promoverNota(int alunoIndex, int novaNota) {
        Aluno aluno = alunos.get(alunoIndex);
        aluno.setNota(novaNota);
    }

    public List<String> listarAlunosDoCurso(String curso) {
        List<String> alunosDoCurso = new ArrayList<>();
        for (Aluno aluno : alunos) {
            if (aluno.getCursos().contains(curso)) {
                alunosDoCurso.add(aluno.getNome());
            }
        }
        return alunosDoCurso;
    }

    public List<String> listarTodosAlunos() {
        List<String> todosAlunos = new ArrayList<>();
        for (Aluno aluno : alunos) {
            todosAlunos.add(aluno.toString());
        }
        return todosAlunos;
    }
}
