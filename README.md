# Laboratório - Semana I: Programação

Bem-vindo ao laboratório da Semana I de programaçãoII!.

## Atividades

Aqui estão os links para acessar os arquivos de cada atividade:

- <a href="https://gitlab.com/programacaoii/lab/labweekI/-/tree/main/src/semanaI/atvI" target="_blank">Atividade 1</a>
- <a href="https://gitlab.com/programacaoii/lab/labweekI/-/tree/main/src/semanaI/atvII" target="_blank">Atividade 2</a>
- <a href="https://gitlab.com/programacaoii/lab/labweekI/-/tree/main/src/semanaI/atvIII" target="_blank">Atividade 3</a>

Clique nos links acima para abrir cada arquivo em uma nova aba do seu navegador.



